#!/bin/sh

rm -rf ./dist
#DEBUG=metalsmith* node ./npm_scripts/metalsmith/dev.js &
node ./npm_scripts/metalsmith/dev.js &
NODE_ENV=development PLATFORM=web webpack-dev-server --hot --inline --open &
wait