#!/bin/sh

rm -rf ./dist
node ./npm_scripts/metalsmith/build.js &
NODE_ENV=production PLATFORM=web webpack &
wait
rm -rf ./dist/assets/scss
