var path         = require('path');
var Metalsmith   = require('metalsmith');
var markdown     = require('metalsmith-markdown');
var layouts      = require('metalsmith-layouts');
var permalinks   = require('metalsmith-permalinks');
var watch        = require('metalsmith-watch');
var assets       = require('metalsmith-assets');
var packageJson  = require(path.resolve('./package.json'));
var globalConfig = require(path.resolve('./src/cfg.js'));

Metalsmith(path.resolve('./src'))
	.metadata({
		packageJson: packageJson,
		cfg: globalConfig,
		__ENV__: 'development'
	})
	.source('./pages')
	.destination(path.resolve('./dist'))
	.use(assets({
		source: './assets',
		destination: './assets'
	}))
	.use(markdown())
	.use(permalinks({
		relative: false
	}))
	.use(layouts({
		engine: 'ejs',
		partials: "globals/blocks"
	}))
	.use(watch({
		paths: {
			"layouts/**/*.html": "**/*.*",
			"pages/**/*.md": "**/*.*"
		},
		livereload: true
	}))
	.build(function(err, files) {
		if (err) { 
			throw err; 
		}
	});

