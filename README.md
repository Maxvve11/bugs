# StatiGen 0.0.1

```
[ ] Add webpack plugins
[ ] Review require function
[ ] eslint / esdoc
[ ] Project components architecture
[ ] Project css architecture
[ ] Test on linux / windows / osx
[ ] Common UI gallery
[ ] Git submodules (for assets)
[ ] Review separated bundling (html, css, js)
```
